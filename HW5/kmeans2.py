import random
import time
import math
import pandas as pd
import time
start_time = time.time()

football = [[3, 5],
            [3, 4],
            [2, 8],
            [2, 3],
            [6, 2],
            [6, 4],
            [7, 3],
            [7, 4],
            [8, 5],
            [7, 6]]

iris = pd.read_csv("iris.txt")
iris = iris.drop(columns=['4'])
iris_list = iris.values.tolist()
print(iris_list)


def k_means(instances, k, init_centroids):
    result = {}
    if len(init_centroids) < k:
        # randomly select k initial centroids
        random.seed(time.time())
        centroids = random.sample(instances, k)
    else:
        centroids = init_centroids
    # print(centroids)
    prev_centroids = []
    iteration = 0
    while centroids != prev_centroids:
        iteration += 1
        clusters = assign_all(instances, centroids)
        prev_centroids = centroids
        centroids = compute_centroids(clusters, centroids)
        withinss = compute_withinss(clusters, centroids)
    result["clusters"] = clusters
    result["centroids"] = centroids
    result["withinss"] = withinss
    sse(result["clusters"], result["centroids"])
    # print(init_centroids)
    print("Clusters: " + str(result["clusters"]))
    print("Centroids: " + str(result["centroids"]))
    return result


def compute_withinss(clusters, centroids):
    result = 0
    for i in range(len(centroids)):
        centroid = centroids[i]
        cluster = clusters[i]
        for instance in cluster:
            result += distance(centroid, instance)
    return result


def mean_instance(name, instance_list):
    num_instances = len(instance_list)
    if num_instances == 0:
        return
    num_attributes = len(instance_list[0])
    means = [name] + [0] * (num_attributes-1)
    for instance in instance_list:
        for i in range(1, num_attributes):
            means[i] += instance[i]
    for i in range(1, num_attributes):
        means[i] /= float(num_instances)
    return tuple(means)


def mean(c_list):
    total = 0
    for x in c_list:
        total = total + x
    m = total / len(c_list)
    return m


def compute_centroids(clusters, cents):
    centroids = []
    count = 0
    for c in clusters:
        # name = "centroid" + str(i)
        centroid = []
        if len(c) == 0:
            #cent = cents[count]
            #cent[0] = cent[0]+0.1
            #print(cent)
            centroids.append([0,0,0,0])
        else:
            length = len(c[0])
            for i in range(0, length):
                c_list = []
                for x in c:
                    # calc mean of x or y of a cluster
                    # print(x[i])
                    c_list.append(x[i])
                centroid.append(mean(c_list))
            centroids.append(centroid)
        count = count + 1
    return centroids


def assign_all(instances, centroids):
    clusters = create_empty_list_of_lists(len(centroids))
    for instance in instances:
        cluster_index = assign(instance, centroids)
        clusters[cluster_index].append(instance)
    return clusters


def create_empty_list_of_lists(num_sub_lists):
    my_list = []
    for i in range(num_sub_lists):
        my_list.append([])
    return my_list


def assign(instance, centroids):
    min_distance = distance(instance, centroids[0])
    min_distance_index = 0
    for i in range(1, len(centroids)):
        d = distance(instance, centroids[i])
        if d < min_distance:
            min_distance = d
            min_distance_index = i
    return min_distance_index


# Manhattan Distance
def manhattan(instance1, instance2):
    sum_squares = 0
    for i in range(0, len(instance1)):
        sum_squares = sum_squares + abs(float(instance1[i]) - float(instance2[i]))
    return sum_squares


def norm(val):
    return math.sqrt(sum([i**2 for i in val]))


# Cosine
def cosine(instance1, instance2):
    top = sum([i * j for (i, j) in zip(instance1, instance2)])
    return 1 - (top / (norm(instance1) * norm(instance2)))

    #top = 0
    #a = 0
    #b = 0
    #for i in range(0, len(instance1)):
    #    top = top + (instance1[i] * instance2[i])
    #    a = a + (instance1[i]**2)
    #    b = b + (instance2[i]**2)
    #bottom = math.sqrt(a) * math.sqrt(b)
    #dist = top / bottom
    #return dist

# Generalized Jaccard
def jaccard(instance1, instance2):
    min_d = 0
    max_d = 0
    for i in range(0, len(instance1)):
        if instance1[i] < instance2[i]:
            min_d = min_d + instance1[i]
            max_d = max_d + instance2[i]
        else:
            min_d = min_d + instance2[i]
            max_d = max_d + instance1[i]
    dist = min_d / max_d
    return 1 - dist

# Euclidean
def e_dist(instance1, instance2):
    dist = 0
    for i in range(0, len(instance1)):
        dist = dist + (float(instance1[i]) - float(instance2[i]))**2
    dist = math.sqrt(dist)
    return dist


# DISTANCE TO USE
def distance(instance1, instance2):
    dist = 0
    for i in range(0, len(instance1)):
        dist = dist + (float(instance1[i]) - float(instance2[i]))**2
    dist = math.sqrt(dist)
    return dist


# Repeats k-means clustering n times, and returns the clustering
# with the smallest withinss
def repeated_k_means(instances, k, n):
    best_clustering = {"withinss": float("inf")}
    # best_clustering["withinss"] = float("inf")
    for i in range(1, n+1):
        print("k-means trial %d," % i)
        trial_clustering = k_means(instances, k, init_centroids)
        print("withinss: %.1f" % trial_clustering["withinss"])
        if trial_clustering["withinss"] < best_clustering["withinss"]:
            best_clustering = trial_clustering
            min_withinss_trial = i
    print("Trial with minimum withinss:", min_withinss_trial)
    return best_clustering


# generate centroids
def start_centers(instances, k):
    random.seed(time.time())
    centroids = random.sample(instances, k)
    print("start centroids: " + str(centroids))
    return centroids


def sse(clusters, centroids):
    # print("SSE")
    # print(clusters)
    # print(centroids)
    # print(len(centroids))
    # sum of squared differences
    sum = 0
    for i in range(len(centroids)):
        # print(clusters[i])
        # print(centroids[i])
        for point in clusters[i]:
            sum = sum + (distance(point, centroids[i]))**2
    result = math.sqrt(sum)
    print("SSE result: ")
    print(result)


def majority(clusters):
    majorities = []
    df = pd.read_csv("iris.txt")
    for cluster in clusters:
        major = [0,0,0]
        for c in cluster:
            result = df.loc[(df['0'] == c[0]) & (df['1'] == c[1]) & (df['2'] == c[2]) & (df['3'] == c[3])]
            result = result.iloc[0]['4']
            if result == "Iris-versicolor":
                major[0] += 1
            elif result == "Iris-virginica":
                major[1] += 1
            elif result == "Iris-setosa":
                major[2] += 1
        majorities.append(major)
    return majorities


# init_centroids = [()()] list of tuples
# init_centroids = [[3, 3], [8, 3]]
# repeated_k_means(football, 2, 4)

# -----------------------------------------------------------
# Problem 2
# -----------------------------------------------------------
# K = 3


init_centroids = start_centers(iris_list, 3)
clusts = repeated_k_means(iris_list, 3, 5)
print(clusts)
print("--- %s seconds ---" % (time.time() - start_time))
classes = majority(clusts['clusters'])
print(classes)