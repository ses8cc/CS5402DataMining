from sklearn.naive_bayes import GaussianNB
from sklearn import tree

gnb = GaussianNB()

# Home = 1, Away = 0
# In = 1, Out = 0
# Media are numbers
# Win = 1, Lose = 0
X = [[1, 0, 1],
     [0, 0, 4],
     [1, 1, 1],
     [1, 0, 1],
     [0, 1, 4],
     [1, 0, 1],
     [1, 1, 1],
     [0, 0, 4],
     [0, 0, 4],
     [1, 0, 1],
     [0, 0, 1],
     [0, 1, 3],
     [0, 0, 4],
     [1, 0, 1],
     [1, 0, 1],
     [1, 0, 1],
     [1, 0, 2],
     [0, 0, 4],
     [1, 1, 1],
     [1, 0, 1],
     [1, 0, 5],
     [1, 0, 1],
     [1, 1, 1],
     [0, 1, 4]]
Y = [1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0]

naive = gnb.fit(X, Y)

pred_data = [[1, 0, 1],
             [1, 1, 1],
             [0, 0, 2],
             [0, 0, 3],
             [1, 0, 1],
             [0, 0, 4],
             [1, 1, 1],
             [1, 0, 1],
             [1, 0, 1],
             [0, 1, 4],
             [1, 0, 1],
             [0, 1, 4]]

results = naive.predict(pred_data)
print("predictions: " + str(results))

expected = [1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0]

# Analysis
tp = 0
fp = 0
tn = 0
fn = 0
tot = len(results)

for x in range(len(results)):
    if results[x] == 0:
        if expected[x] == 0:
            tn = tn + 1
        else:
            fn = fn + 1
    elif results[x] == 1:
        if expected[x] == 0:
            fp = fp + 1
        else:
            tp = tp + 1

print("tp: " + str(tp))
print("fp: " + str(fp))
print("tn: " + str(tn))
print("fn: " + str(fn))

acc = (tp + tn) / tot
prec = tp / (tp + fp)
recall = tp / (tp + fn)
f1score = 2 * (prec * recall) / (prec + recall)

print("accuracy: " + str(acc))
print("precision: " + str(prec))
print("recall: " + str(recall))
print("f1 score: " + str(f1score))
