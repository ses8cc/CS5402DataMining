
# coding: utf-8

# In[140]:


#Q7: distribution of numerical features
import pandas as pd

train_df = pd.read_csv('C:/Users/Sarah/Downloads/Data Mining/Titanic/train.csv')

train_df.describe(include='number')


# In[12]:


#Q8: Distribution of Categorical Features
train_df.describe(exclude='number')


# In[138]:


#Q9 Correlation between Pclass =1 and survived
corr = train_df.loc[train_df['Survived'] == 1]
corr['Pclass'].hist()


# In[142]:


#Q10 Female stats
female_df = train_df.loc[train_df['Sex'] == 'female']
female_df.describe()


# In[17]:


#Q10 Male Stats
male_df = train_df.loc[train_df['Sex'] == 'male']
male_df.describe()


# In[144]:


#Q11: Survived Histogram
import plotly
import plotly.plotly as py
import plotly.tools as tls

import matplotlib.pyplot as plt
import numpy as np

survived_df = train_df.loc[train_df['Survived'] == 1]

survived_df['Age'].hist()


# In[31]:


#Q11: Not Survived Histogram
not_surv_df = train_df.loc[train_df['Survived'] == 0]

not_surv_df['Age'].hist()


# In[36]:


#Q12: Histogram showing correlation between Pclass value and survival
# blue = Pclass = 1
# orange = Pclass = 2
# green = Pclass = 3
c1_surv_df = survived_df.loc[survived_df['Pclass'] == 1]
c2_surv_df = survived_df.loc[survived_df['Pclass'] == 2]
c3_surv_df = survived_df.loc[survived_df['Pclass'] == 3]
c1_not_surv_df = not_surv_df.loc[not_surv_df['Pclass'] == 1]
c2_not_surv_df = not_surv_df.loc[not_surv_df['Pclass'] == 2]
c3_not_surv_df = not_surv_df.loc[not_surv_df['Pclass'] == 3]

c1_surv_df['Age'].hist()
c2_surv_df['Age'].hist()
c3_surv_df['Age'].hist()


# In[38]:


#Q12: Histogram showing correlation between Pclass value and not survival
# blue = Pclass = 3
# orange = Pclass = 1
# green = Pclass = 2
c3_not_surv_df['Age'].hist()
c1_not_surv_df['Age'].hist()
c2_not_surv_df['Age'].hist()


# In[39]:


#Q12: Prep work
embarkS_surv_df = survived_df.loc[survived_df['Embarked'] == 'S']
embarkC_surv_df = survived_df.loc[survived_df['Embarked'] == 'C']
embarkQ_surv_df = survived_df.loc[survived_df['Embarked'] == 'Q']
embarkS_not_surv_df = not_surv_df.loc[not_surv_df['Embarked'] == 'S']
embarkC_not_surv_df = not_surv_df.loc[not_surv_df['Embarked'] == 'C']
embarkQ_not_surv_df = not_surv_df.loc[not_surv_df['Embarked'] == 'Q']


# In[76]:


#Q12: Histogram plotting showing relation bewteen sex, embark location, fare cost, and if they survived.
fig, axes = plt.subplots(3, 2, sharex = True, sharey = True)

embarS_surv = ((embarkS_surv_df.groupby(['Sex'])).mean()['Fare']).tolist()
axes[0,0].set_title("Survived, S")
axes[0,0].bar(["female","male"], embarS_surv)

embarC_surv = ((embarkC_surv_df.groupby(['Sex'])).mean()['Fare']).tolist()
axes[1,0].set_title("Survived, C")
axes[1,0].bar(["female","male"], embarC_surv)

embarQ_surv = ((embarkQ_surv_df.groupby(['Sex'])).mean()['Fare']).tolist()
axes[2,0].set_title("Survived, Q")
axes[2,0].bar(["female","male"], embarQ_surv)

embarS_not_surv = ((embarkS_not_surv_df.groupby(['Sex'])).mean()['Fare']).tolist()
axes[0,1].set_title("Not Survived, S")
axes[0,1].bar(["female","male"], embarS_not_surv)

embarC_not_surv = ((embarkC_not_surv_df.groupby(['Sex'])).mean()['Fare']).tolist()
axes[1,1].set_title("Not Survived, C")
axes[1,1].bar(["female","male"], embarC_not_surv)

embarQ_not_surv = ((embarkQ_not_surv_df.groupby(['Sex'])).mean()['Fare']).tolist()
axes[2,1].set_title("Not Survived, Q")
axes[2,1].bar(["female","male"], embarQ_not_surv)


# In[155]:


#Q14: ticket duplication
unique = train_df['Ticket'].unique()

counting = {key: 0 for (key) in unique}
for t in train_df['Ticket']:
    if t in counting.keys():
        counting[t] = counting[t] + 1
        
uniqTot = 0;
for key in counting:
    if counting[key] > 1:
        uniqTot = uniqTot + 1
        
print(uniqTot)


# In[151]:


# Q15 Cabin feature count and nulls for train data
train_df.describe(exclude='number')


# In[152]:


# Q16 Added Gender Feature based on Sex feature
genders = []
for s in train_df['Sex']:
    if s == 'female':
        genders.append(1)
    else:
        genders.append(0)

train_df['Gender'] = genders
print(train_df)


# In[118]:


# Q17: fill null values for age feature by generating random umbers between mean and std. dev. 
import math;
import random;
ages = []
for entry in train_df['Age']: 
    if math.isnan(entry):
        ages.append(random.randint(15,46))
    else :
        ages.append(entry)
train_df['Age'] = ages
print(train_df)


# In[123]:


# Q18: fill blank Embarked with most occuring value (S)
bark = []
for entry in train_df['Embarked']: 
    if entry != 'S' or entry != 'Q' or entry != 'C':
        bark.append('S')
    else :
        bark.append(entry)
train_df['Embarked'] = bark
print(train_df)


# In[132]:


# Q19: fill empty fare values with the mode of all fare entries
import math;

test_df = pd.read_csv('C:/Users/Sarah/Downloads/Data Mining/Titanic/test.csv')

mode = test_df['Fare'].mode()

fares = []
for entry in test_df['Fare']: 
    if math.isnan(entry):
        fares.append(mode)
    else :
        fares.append(entry)
test_df['Fare'] = fares
print(test_df)


# In[136]:


# Q20: convert Fare feature into ordinal values based on Fareband 

fareband = []
for entry in train_df['Fare']: 
    if entry > -0.001 and entry <= 7.91:
        fareband.append(0)
    elif entry > 7.91 and entry <= 14.454:
        fareband.append(1)
    elif entry > 14.454 and entry <= 31.0:
        fareband.append(2)
    elif entry > 31.0 and entry <= 512.329:
        fareband.append(3) 
    else:
        fareband.append(1) # where the mode was if the value is blank
train_df['Fare'] = fareband
print(train_df)

