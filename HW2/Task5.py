from sklearn import tree
import graphviz
# C4.5 implementation from https://github.com/zhuang-hao-ming/c4.5-python
from c45python import c45

# Task 5: ID3 Decision Tree

# Home = 1, Away = 0
# In = 1, Out = 0
# Media are numbers
# Win = 1, Lose = 0
X = [[1, 0, 1],
     [0, 0, 4],
     [1, 1, 1],
     [1, 0, 1],
     [0, 1, 4],
     [1, 0, 1],
     [1, 1, 1],
     [0, 0, 4],
     [0, 0, 4],
     [1, 0, 1],
     [0, 0, 1],
     [0, 1, 3],
     [0, 0, 4],
     [1, 0, 1],
     [1, 0, 1],
     [1, 0, 1],
     [1, 0, 2],
     [0, 0, 4],
     [1, 1, 1],
     [1, 0, 1],
     [1, 0, 5],
     [1, 0, 1],
     [1, 1, 1],
     [0, 1, 4]]

Y = [1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0]
id3 = tree.DecisionTreeClassifier(criterion='entropy')
id3 = id3.fit(X,Y)

print(id3.predict([[1, 0, 1],
                    [1, 1, 1],
                    [0, 0, 2],
                    [0, 0, 3],
                    [1, 0, 1],
                    [0, 0, 4],
                    [1, 1, 1],
                    [1, 0, 1],
                    [1, 0, 1],
                    [0, 1, 4],
                    [1, 0, 1],
                    [0, 1, 4]]))

features = ["Home/Away", "Opp in AP Top 25?","Media"]
classes = ["Lose", "Win"]

dot_data = tree.export_graphviz(id3, out_file=None, feature_names=features, class_names=classes)
graph = graphviz.Source(dot_data)
graph.render("task5ID3")

# get the tree for C4.5
test_set = [[1, 0, 1, 1],
     [0, 0, 4, 1],
     [1, 1, 1, 1],
     [1, 0, 1, 1],
     [0, 1, 4, 0],
     [1, 0, 1, 1],
     [1, 1, 1, 1],
     [0, 0, 4, 1],
     [0, 0, 4, 1],
     [1, 0, 1, 1],
     [0, 0, 1, 1],
     [0, 1, 3, 0],
     [0, 0, 4, 0],
     [1, 0, 1, 1],
     [1, 0, 1, 0],
     [1, 0, 1, 0],
     [1, 0, 2, 1],
     [0, 0, 4, 0],
     [1, 1, 1, 0],
     [1, 0, 1, 1],
     [1, 0, 5, 0],
     [1, 0, 1, 1],
     [1, 1, 1, 0],
     [0, 1, 4, 0]]
labels = ["Home/Away", "In/Out", "Media"]

c45tree = c45.create_tree(test_set, labels)
print(c45tree)
