from sklearn import tree
import graphviz
# C4.5 implementation from https://github.com/zhuang-hao-ming/c4.5-python
from c45python import c45

# TASK 4
# ------------------------------------------------------------------------
# Question 1

# Home = 1, Away = 0
# In = 1, Out = 0
# Media are numbers
# Win = 1, Lose = 0
X = [[1, 0, 1], [1, 1, 1], [0, 0, 2], [0, 0, 3], [1, 0, 1], [0, 0, 4]]
Y=[1, 0, 1, 1, 1, 1]
id3 = tree.DecisionTreeClassifier(criterion='entropy')
id3 = id3.fit(X,Y)
cast = tree.DecisionTreeClassifier()
cast = cast.fit(X,Y)

print(id3.predict([[1, 1, 1],
             [1, 0, 1],
             [1, 0, 1],
             [0, 1, 4],
             [1, 0, 1],
             [0, 1, 4]]))

print(cast.predict([[1, 1, 1],
             [1, 0, 1],
             [1, 0, 1],
             [0, 1, 4],
             [1, 0, 1],
             [0, 1, 4]]))

features = ["Home/Away", "Opp in AP Top 25?","Media"]
classes = ["Lose", "Win"]

dot_data = tree.export_graphviz(id3, out_file=None, feature_names=features, class_names=classes)
graph = graphviz.Source(dot_data)
graph.render("data1ID3")

dot_data = tree.export_graphviz(cast, out_file=None, feature_names=features, class_names=classes)
graph = graphviz.Source(dot_data)
graph.render("data1Cast")

# c4.5
# get the tree for C4.5
test_set = [[1, 0, 1, 1], [1, 1, 1, 0], [0, 0, 2, 1], [0, 0, 3, 1], [1, 0, 1, 1], [0, 0, 4, 1]]
labels = ["Home/Away", "In/Out", "Media"]

c45tree = c45.create_tree(test_set, labels)
print(c45tree)
# ------------------------------------------------------------------------
# Question 2

# Outlook: Sunny = 2, Overcast = 1, Rainy = 0
# Temp: Hot = 2, Mild = 1, Cool = 0
# Humidity: High = 1, Normal = 0
# Windy: True = 1, False = 0
X2 = [[2, 2, 1, 0],
      [2, 2, 1, 1],
      [1, 2, 1, 0],
      [0, 1, 1, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 1],
      [1, 0, 0, 1],
      [2, 1, 1, 0],
      [2, 0, 0, 0],
      [0, 1, 0, 0],
      [2, 1, 0, 1],
      [1, 1, 1, 1],
      [1, 2, 0, 0],
      [0, 1, 1, 1]]
# Results: Yes = 1, No = 0
Y2 = [0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0]

id32 = tree.DecisionTreeClassifier(criterion='entropy')
id32 = id3.fit(X2, Y2)
cast2 = tree.DecisionTreeClassifier()
cast2 = cast.fit(X2, Y2)

print(id32.predict([[0, 2, 1, 0]]))

print(cast2.predict([[0, 2, 1, 0]]))

# GRAPHING Decision Tree
features2 = ["Outlook", "Temp", "Humidity", "Windy"]
classes2 = ["No", "Yes"]

dot_data2 = tree.export_graphviz(id32, out_file=None, feature_names=features2, class_names=classes2)
graph2 = graphviz.Source(dot_data2)
graph2.render("data2ID3")

dot_data2 = tree.export_graphviz(cast2, out_file=None, feature_names=features2, class_names=classes2)
graph2 = graphviz.Source(dot_data2)
graph2.render("data2Cast")


# get the tree for C4.5
test_set = [[2, 2, 1, 0, 0],
      [2, 2, 1, 1, 0],
      [1, 2, 1, 0, 1],
      [0, 1, 1, 0, 1],
      [0, 0, 0, 0, 1],
      [0, 0, 0, 1, 0],
      [1, 0, 0, 1, 1],
      [2, 1, 1, 0, 0],
      [2, 0, 0, 0, 1],
      [0, 1, 0, 0, 1],
      [2, 1, 0, 1, 1],
      [1, 1, 1, 1, 1],
      [1, 2, 0, 0, 1],
      [0, 1, 1, 1, 0]]
labels = ["Outlook", "Temp", "Humidity", "Windy"]

c45tree = c45.create_tree(test_set, labels)
print(c45tree)