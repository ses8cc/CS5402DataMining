from surprise import Dataset
from surprise import Reader
from surprise import SVD
from surprise import evaluate, print_perf
from surprise import NMF
from surprise import KNNBasic
import os

import matplotlib.pyplot as plt
import numpy as np

# load data from a file
file_path = os.path.expanduser('C:/Users/Schmittie/PycharmProjects/DataMining/HW6/restaurant_ratings.txt')
reader = Reader(line_format='user item rating timestamp', sep='\t')
data = Dataset.load_from_file(file_path, reader=reader)

data.split(n_folds=3)

# part 5: RMSE and MAE of SVD
algo = SVD()
perf = evaluate(algo, data, measures=['RMSE','MAE'])
print_perf(perf)

# part 6: RMSE and MAE of PMF
algo = SVD(biased=False)
perf = evaluate(algo, data, measures=['RMSE','MAE'])
print_perf(perf)

# part 7: RMSE and MAE of NMF
algo = NMF()
perf = evaluate(algo, data, measures=['RMSE','MAE'])
print_perf(perf)

# part 8: RMSE and MAE of user based collaborative filtering
algo = KNNBasic(sim_options={
    'user_based': True
})
perf = evaluate(algo, data, measures=['RMSE','MAE'])
print_perf(perf)

# part 9: RMSE and MAE of item based collaborative filtering
algo = KNNBasic(sim_options={
    'user_based': False
})
perf = evaluate(algo, data, measures=['RMSE','MAE'])
print_perf(perf)

# part 14: How do different similarities impact User Cf and Itm CF
print("\n\nPART 14 Comparing USER CF and ITEM CF\n\n")
# -----USER CF------
# MSD
algo = KNNBasic(sim_options={
    'name': 'MSD',
    'user_based': True
})
perf = evaluate(algo, data, measures=['RMSE','MAE'])
print_perf(perf)

# cosine similarity
algo = KNNBasic(sim_options={
    'name': 'cosine',
    'user_based': True
})
perf = evaluate(algo, data, measures=['RMSE','MAE'])
print_perf(perf)

# Pearson similarity
algo = KNNBasic(sim_options={
    'name': 'pearson',
    'user_based': True
})
perf = evaluate(algo, data, measures=['RMSE','MAE'])
print_perf(perf)

# -----ITEM CF------
# MSD
algo = KNNBasic(sim_options={
    'name': 'MSD',
    'user_based': False
})
perf = evaluate(algo, data, measures=['RMSE','MAE'])
print_perf(perf)

# cosine similarity
algo = KNNBasic(sim_options={
    'name': 'cosine',
    'user_based': False
})
perf = evaluate(algo, data, measures=['RMSE','MAE'])
print_perf(perf)

# Pearson similarity
algo = KNNBasic(sim_options={
    'name': 'pearson',
    'user_based': False
})
perf = evaluate(algo, data, measures=['RMSE','MAE'])
print_perf(perf)

# Plot results from part 14: ------------------------------------

