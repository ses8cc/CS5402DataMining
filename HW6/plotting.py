import matplotlib.pyplot as plt

# User CF RMSE
plt.plot(['MSD', 'Cosine', 'Pearson'], [0.9884, 1.0213, 1.0204], 'ro')
# Item CF RMSE
plt.plot(['MSD', 'Cosine', 'Pearson'], [0.9854, 1.0363, 1.0484], 'bo')
# User CF MAE
plt.plot(['MSD', 'Cosine', 'Pearson'], [0.7811, 0.8084, 0.8100], 'rs')
# Item CF MAE
plt.plot(['MSD', 'Cosine', 'Pearson'], [0.7806, 0.8224, 0.8386], 'bs')
#plt.axis([, ])
plt.xlabel('Similarity Measure')
plt.ylabel('Fit Measure')
plt.title('Plot of User Cf and Item CF for different Similarity Measures')
plt.show()