from surprise import Dataset
from surprise import Reader
from surprise import SVD
from surprise import evaluate, print_perf
from surprise import NMF
from surprise import KNNBasic
import os

# load data from a file
file_path = os.path.expanduser('C:/Users/Schmittie/PycharmProjects/DataMining/HW6/restaurant_ratings.txt')
reader = Reader(line_format='user item rating timestamp', sep='\t')
data = Dataset.load_from_file(file_path, reader=reader)

data.split(n_folds=3)

# Part 15: How number of neighbors impacts User CF and Item CF

for i in range(1, 21):
    print("For K = ", i)
    # RMSE and MAE of user based collaborative filtering
    algo = KNNBasic(k=i, sim_options={
        'user_based': True
    })
    perf = evaluate(algo, data, measures=['RMSE'])
    print_perf(perf)

    # RMSE and MAE of item based collaborative filtering
    algo = KNNBasic(k=i, sim_options={
        'user_based': False
    })
    perf = evaluate(algo, data, measures=['RMSE'])
    print_perf(perf)