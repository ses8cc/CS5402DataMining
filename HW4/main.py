import pandas as pd
import csv
import re
# randomly split the data matrix into five exclusive partitions
# (namely part1, part2, part3, part4, par5) in terms of rows.
with open('iris_data.csv', 'r') as f:
    reader = csv.reader(f)
    iris = list(reader)

p1 = ""
p2 = ""
p3 = ""
p4 = ""
p5 = ""

count = 0
size = 0
for x in iris:
    mod = count % 5
    if mod == 0:
        x1 = re.sub("[\[\]' ]", "", str(x))
        p1 = p1 + str(x1) + "\n"
    elif mod == 1:
        x1 = re.sub("[\[\]' ]", "", str(x))
        p2 = p2 + str(x1) + "\n"
    elif mod == 2:
        x1 = re.sub("[\[\]' ]", "", str(x))
        p3 = p3 + str(x1) + "\n"
    elif mod == 3:
        x1 = re.sub("[\[\]' ]", "", str(x))
        p4 = p4 + str(x1) + "\n"
    elif mod == 4:
        x1 = re.sub("[\[\]' ]", "", str(x))
        p5 = p5 + str(x1) + "\n"
    count = count + 1

test_file = open("testing.csv", "w")
test_file.write(p5)
# remove classification from test values!!!!!!!!!!

train_file = open("training.csv", "w")
train_file.write(p1)
train_file.write(p2)
train_file.write(p3)
train_file.write(p4)
test_file.close()
train_file.close()

# Don't split data matrix in terms of features.%use part1 as testing set, part2+3+4+5 as training set,
# feed the training set to training.py and get a trained decision tree model,
# then feed the testing set to testing.py and get the result
# similarly, you can apply the above step to part2, or part3, or part4, or part5 for testing
# aggregate all the five results into an average performance measurement
