import matplotlib.pyplot as plt

x = (1,2,3,4,5)
at = (0.9666666666666667,0.9666666666666667, 0.8666666666666667, 0.9666666666666667, 0.9333333333333333)
ft = (0.9665831244778613,0.9665831244778613, 0.861111111111111, 0.9665831244778613, 0.9326599326599326)

ak = (1,0.9666666666666667, 0.9333333333333333, 0.9666666666666667, 1)
fk = (1,0.9665831244778613, 0.9326599326599326, 0.9665831244778613, 1)


plt.bar(x,at,align='center') # A bar chart
plt.xlabel('K')
plt.xticks([i for i in range (len(at))])
plt.ylabel('Accuracy for Decision Tree Classifier')
for i in range(len(at)):
    plt.hlines(at[i],0,x[i]) # Here you are drawing the horizontal lines
plt.show()

plt.bar(x,ft,align='center') # A bar chart
plt.xlabel('K')
plt.xticks([i for i in range (len(ft))])
plt.ylabel('F-measure for Decision Tree Classifier')
for i in range(len(ft)):
    plt.hlines(ft[i],0,x[i]) # Here you are drawing the horizontal lines
plt.show()

plt.bar(x,ak,align='center') # A bar chart
plt.xlabel('K')
plt.xticks([i for i in range (len(ak))])
plt.ylabel('Accuracy for kNN')
for i in range(len(ak)):
    plt.hlines(ak[i],0,x[i]) # Here you are drawing the horizontal lines
plt.show()

plt.bar(x,fk,align='center') # A bar chart
plt.xlabel('K')
plt.xticks([i for i in range (len(fk))])
plt.ylabel('F-measure for kNN')
for i in range(len(fk)):
    plt.hlines(fk[i],0,x[i]) # Here you are drawing the horizontal lines
plt.show()