import pickle
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
import csv
# read training set (a CSV file)

with open('training.csv') as f:
    reader = csv.reader(f)
    train = list(reader)

X = []
Y = []
for x in train:
    X.append(x[0:4])
    Y.append(x[4])
print(X)
print(Y)

# train a decision tree with training set
clf = DecisionTreeClassifier(random_state=0)
clf = clf.fit(X, Y)

# save the trained decision tree model to a txt file
tree_file = open("dtc.txt", "wb")
pickle.dump(clf, tree_file)
tree_file.close()

# -------Knn----------
# read training set (a CSV file)
X = []
Y = []
for x in train:
    y = []
    for j in range(len(x)):
            if j < 4:
                y.append(float(x[j]))
            else:
                y.append(x[j])
    X.append(y[0:4])
    Y.append(y[4])
print(X)
print(Y)

# train knn
neigh = KNeighborsClassifier(n_neighbors=8)
neigh.fit(X, Y)

# save the trained decision tree model to a txt file
knn_file = open("knn.txt", "wb")
pickle.dump(neigh, knn_file)
knn_file.close()