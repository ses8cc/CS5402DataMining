from sklearn.metrics import classification_report
from sklearn.metrics import f1_score
import csv
import pickle
from sklearn.tree import DecisionTreeClassifier
# load the trained decision tree model from the txt file
tree_file = open("dtc.txt", "rb")
clt = pickle.load(tree_file)
tree_file.close()

with open('testing.csv') as f:
    reader = csv.reader(f)
    test = list(reader)

XT = []
YT = []
for x in test:
    y = []
    for j in range(len(x)):
            if j < 4:
                y.append(float(x[j]))
            else:
                y.append(x[j])
    XT.append(y[0:4])
    YT.append(y[4])
print(XT)
print(YT)

results = clt.predict(XT)
print("CLT results")
print(results)

correct = 0
for x in range(len(YT)):
    if YT[x] == results[x]:
        correct = correct + 1
    else:
        print(YT[x])
        print(results[x])
print(correct)

# compute Accuracy and f-measur
accuracy = correct / len(results)
print("accuracy: " + str(accuracy))
print(f1_score(YT, results, average='macro'))
print(classification_report(YT, results))
# save the results to a txt file


# TESTING kNN
knn_file = open("knn.txt", "rb")
knn = pickle.load(knn_file)
knn_file.close()

with open('testing.csv') as f:
    reader = csv.reader(f)
    test = list(reader)

XT = []
YT = []
for x in test:
    y = []
    for j in range(len(x)):
            if j < 4:
                y.append(float(x[j]))
            else:
                y.append(x[j])
    XT.append(y[0:4])
    YT.append(y[4])
print(XT)
print(YT)

results = knn.predict(XT)
print("KNN results")
print(results)

correct = 0
for x in range(len(YT)):
    if YT[x] == results[x]:
        correct = correct + 1
    else:
        print(YT[x])
        print(results[x])
print(correct)

# analysis of results
accuracy = correct / len(results)
print("accuracy: " + str(accuracy))
print(f1_score(YT, results, average='macro'))
print(classification_report(YT, results))
