from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import f1_score
import csv
# read training set (a CSV file)

with open('training.csv') as f:
    reader = csv.reader(f)
    train = list(reader)

X = []
Y = []
for x in train:
    y = []
    for j in range(len(x)):
            if j < 4:
                y.append(float(x[j]))
            else:
                y.append(x[j])
    X.append(y[0:4])
    Y.append(y[4])
print(X)
print(Y)

acc = []
fm = []

for r in range(1, 40):
    # train knn
    neigh = KNeighborsClassifier(n_neighbors=r)
    neigh.fit(X, Y)

    # TESTING kNN
    with open('testing.csv') as f:
        reader = csv.reader(f)
        test = list(reader)

    XT = []
    YT = []
    for x in test:
        y = []
        for j in range(len(x)):
            if j < 4:
                y.append(float(x[j]))
            else:
                y.append(x[j])
        XT.append(y[0:4])
        YT.append(y[4])
    print(XT)
    print(YT)

    results = neigh.predict(XT)
    print("results")
    print(results)

    correct = 0
    for x in range(len(YT)):
        if YT[x] == results[x]:
            correct = correct + 1
        else:
            print(YT[x])
            print(results[x])
    print(correct)

    accuracy = correct / len(results)
    fmeasure = f1_score(YT, results, average='macro')
    acc.append(accuracy)
    fm.append(fmeasure)

print(acc)
print(fm)